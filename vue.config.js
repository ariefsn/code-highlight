const host = process.env.VUE_APP_HOST || '127.0.0.1'
const port = process.env.VUE_APP_PORT || 4000
const path = process.env.VUE_PUBLIC_PATH || '/'

module.exports = {
  publicPath: path,
  runtimeCompiler: true,
  devServer: {
    host: host,
    port: port
  },
  transpileDependencies: [
    'vuetify'
  ]
}
