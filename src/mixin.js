import { mapActions } from 'vuex'
import dayjs from 'dayjs'

export default {
  methods: {
    ...mapActions(['setNotify']),
    getUserId () {
      return localStorage.getItem('userId')
    },
    getUserName () {
      return localStorage.getItem('userName')
    },
    showNotify (text = '', color = null) {
      this.setNotify({ show: true, text, color })
    },
    resetNotify () {
      this.setNotify({ show: false, text: '', color: null })
    },
    formatDate (date, format) {
      if (!date) return ''

      return dayjs(date).format(format)
    }
  }
}
